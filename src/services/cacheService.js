var localStorage = window.localStorage
const get = (item) => {
  return localStorage.getItem(item)
}
const set = (key, item) => {
  localStorage.setItem(key, item)
}
const remove = (item) => {
  localStorage.removeItem(item)
}

export default {
  get,
  set,
  remove
}
