import data from './dataService'

const getContact = (id) => {
  return data.getContact(id)
}

const deleteContact = (id) => {
  return data.deleteContact(id)
}

const updateContact = (item) => {
  return data.updateContact(item)
}

const addContact = (item) => {
  return data.addContact(item)
}

export default {
  getContact,
  deleteContact,
  updateContact,
  addContact
}
