import data from './dataService'

const getContacts = () => {
  return data.getContacts()
}

export default {
  getContacts
}
