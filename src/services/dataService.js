import cache from './cacheService'
import _ from 'underscore'
import contacts from '../../static/api/contacts.json'
var key = 'contactList'
const getContacts = () => {
  if (!cache.get(key)) {
    setContacts(contacts)
    return contacts
  } else {
    return JSON.parse(cache.get(key))
  }
}

const setContacts = (items) => {
  cache.set(key, JSON.stringify(items))
}

const getContact = (id) => {
  var contacts = getContacts()
  return _.findWhere(contacts, { id: id })
}

const deleteContacts = () => {
  setContacts('')
}

const deleteContact = (item) => {
  var contacts = getContacts()
  if (item) {
    contacts = _.reject(contacts, contact => contact.id === item.id)
    setContacts(contacts)
    return getContacts()
  }
  return false
}

const addContact = (contact) => {
  var contacts = getContacts()
  contacts.push(contact)
  setContacts(contacts)
  return getContacts()
}

const updateContact = (contact) => {
  var contacts = getContacts()
  _.each(contacts, function (item, index) {
    if (item.id === contact.id) {
      contacts[index] = contact
    }
  })
  setContacts(contacts)
  return getContacts()
}

export default {
  getContacts,
  getContact,
  deleteContacts,
  deleteContact,
  addContact,
  updateContact
}
