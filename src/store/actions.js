import contactsService from '../services/contactsService'

export const fetchContacts = ({ commit }) => {
  commit('FETCH_CONTACTS', contactsService.getContacts())
}

export const setMessage = ({ commit }, value) => {
  setTimeout(() => { commit('SET_MESSAGE', false) }, 800)
  commit('SET_MESSAGE', value)
}

export const setModal = ({ commit }, value) => {
  commit('SET_MODAL', value)
}
export const cancel = ({ dispatch, commit }) => {
  dispatch('setModal', false)
}
export const confirm = ({ dispatch, state }) => {
  if (!state.modal) return
  switch (state.modal.type) {
    case 'delete':
      dispatch('deleteContact')
  }
}
