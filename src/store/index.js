import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'
import contact from './modules/contact'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState({ storage: window.localStorage })],
  state: {
    contacts: [],
    message: false,
    modal: false
  },
  actions,
  getters,
  mutations,
  modules: {
    contact
  },
  strict: true
})
