import contactService from '../../services/contactService'
const state = {
  contact: {
    fullName: '',
    email: '',
    favourite: false,
    picture: '',
    numbers: [
    ]
  },
  tempContact: {
    fullName: '',
    email: '',
    favourite: false,
    picture: '',
    numbers: [
    ],
    id: Math.floor((Math.random() * 10000000000000000) + 1000)
  },
  editing: false,
  adding: false
}

// getters
const getters = {
  contact: state => state.contact,
  tempContact: state => state.tempContact,
  adding: state => state.adding,
  editing: state => state.editing
}

// actions
const actions = {
  setContact ({ commit }, contact) {
    commit('SET_CONTACT', contact)
  },
  updateContact ({ commit }) {
    commit('UPDATE_CONTACT')
  },
  updateTempContact ({ commit }, value) {
    commit('UPDATE_TEMP_CONTACT', value)
  },
  updateTempContactNumber ({ commit }, value) {
    commit('UPDATE_TEMP_CONTACT_NUMBER', value)
  },
  favouriteContact ({ dispatch, commit }) {
    commit('FAV_CONTACT')
    dispatch('updateContact')
  },
  deleteContact ({ dispatch, commit }) {
    var contacts = contactService.deleteContact(state.contact)
    dispatch('setModal', false)
    dispatch('setMessage', 'Contact is deleted')
    commit('FETCH_CONTACTS', contacts)
    commit('DELETE_CONTACT')
  },
  deleteNumber ({ commit }, value) {
    commit('DELETE_NUMBER', value)
  },
  editContact ({ commit }, item) {
    commit('EDIT_CONTACT', item)
  },
  editingContact ({ commit }) {
    commit('EDITING')
  },
  addingContact ({ commit }) {
    commit('ADDING')
  },
  saveContact ({ commit, state }) {
    var contacts = state.editing ? contactService.updateContact(state.tempContact) : contactService.addContact(state.tempContact)
    commit('FETCH_CONTACTS', contacts)
    commit('SAVE')
  },
  cancelContact ({ commit, state }) {
    commit('CANCEL')
  },
  addNumber ({ commit }) {
    commit('ADD_NUMBER')
  }
}

// mutations
const mutations = {
  FAV_CONTACT (state) {
    state.contact.favourite ? state.contact.favourite = false : state.contact.favourite = true
  },
  SET_CONTACT (state, value) {
    state.contact = value
  },
  EDIT_CONTACT (state, value) {
    state.contact = value
    contactService.updateContact(value)
  },
  DELETE_CONTACT (state) {
    state.contact = {}
    state.tempContact = {}
  },
  UPDATE_TEMP_CONTACT (state, value) {
    state.tempContact[value.key] = value.value
  },
  UPDATE_TEMP_CONTACT_NUMBER (state, value) {
    state.tempContact.numbers[value.index][value.type] = value.value
  },
  UPDATE_CONTACT (state) {
    contactService.updateContact(state.contact)
  },
  ADDING (state) {
    state.adding = true
    state.editing = false
    state.tempContact = {
      fullName: '',
      email: '',
      favourite: false,
      picture: '',
      numbers: [
      ],
      id: Math.floor((Math.random() * 10000000000000000) + 1000)
    }
  },
  EDITING (state) {
    state.editing = true
    state.adding = false
    state.tempContact = { ...state.contact }
  },
  SAVE (state) {
    state.editing = false
    state.adding = false
    state.contact = { ...state.tempContact }
  },
  CANCEL (state) {
    state.editing = false
    state.adding = false
    state.tempContact = {}
  },
  ADD_NUMBER (state) {
    state.tempContact.numbers = state.tempContact.numbers || []
    state.tempContact.numbers.push({ value: '', name: '', id: Math.floor((Math.random() * 10000000000000000) + 1000) })
  },
  DELETE_NUMBER (state, value) {
    state.tempContact.numbers.splice(state.tempContact.numbers.findIndex(function (i) {
      return i.id === value
    }), 1)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
