export const FETCH_CONTACTS = (state, value) => {
  state.contacts = value
}

export const SET_MESSAGE = (state, value) => {
  state.message = value
}

export const SET_MODAL = (state, value) => {
  state.modal = value
}
