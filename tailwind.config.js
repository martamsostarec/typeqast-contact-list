module.exports = {
    important: true,
    theme: {
        colors: {
            teal: {
                '100': '#86c9ce',
                '200': '#51b1ba',
                '300': '#34a4b0'
            },
            gray: {
                '100': '#eaeded',
                '200': '#f1f3f3',
                '300': '#ebebeb',
                '400': '#bfc7c6',
                '500': '#bfc7c6'
            },
            message: {
                '100': '#cdc992',
                '200': '#cdc972',
                '300': '#cdc953',
                '400': '#a5a058'
            }
        },
        borderWidth: {
            default: '1.5px',
            '0': '0',
            '2': '2px',
            '4': '4px',
        }
    },
    variants: {
        opacity: ['responsive', 'hover']
    }
}